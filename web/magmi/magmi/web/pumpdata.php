<?php
require_once("../inc/magmi_defs.php");
//Datapump include
require_once("../integration/inc/magmi_datapump.php");

$dp=Magmi_DataPumpFactory::getDataPumpInstance("productimport");
$dp->beginImportSession("default","create");
echo 'start for \n';
echo  date("Y/m/d h:i:s").'\n';

for($i=0;$i<18000;$i++)
{

    echo 'start insert:\n';
    echo "===========start $i=================\n";
    echo '=================================';
    $newProductData = array(
        'type'          => 'simple',
        'sku'           => "A001-".$i,
        'qty'           => 1000,
        'color'         => 'Blue',
        'price'         => 10,
        'name'          => 'A001-'.$i,
        'tax_class_id'  => 1,
        'is_in_stock'   => 1,
        'store'         => 'admin'
    );
    $dp->ingest($newProductData);

    echo '==================================';
    echo "===========end $i=================\n";
}

echo 'end for \n';

echo  date("Y/m/d h:i:s");
/*$newProductData = array(
    'type'          => 'simple',
    'sku'           => "A001-2",
    'qty'           => 1000,
    'color'         => 'Blue',
    'price'         => 10,
    'name'          => 'A001-2',
    'tax_class_id'  => 1,
    'is_in_stock'   => 1,
    'store'         => 'admin'
);
$dp->ingest($newProductData);
$newProductData = array(
    'type'          => 'simple',
    'sku'           => "A001-1",
    'qty'           => 1000,
    'color'         => 'Indigo',
    'price'         => 10,
    'name'          => 'A001-1',
    'tax_class_id'  => 1,
    'is_in_stock'   => 1,
    'store'         => 'admin'
);
$dp->ingest($newProductData);
$newProductData = array(
    'type'          => 'configurable',
    'sku'           => "A001",
    'qty'           => 1000,
    'price'         => 10,
    'simples_skus'  => 'A001-2,A001-1',
    'configurable_attributes' => 'color',
    'name'          => 'TREAD JEANS',
    'tax_class_id'  => 1,
    'is_in_stock'   => 1,
    'store'         => 'admin'
);
$dp->ingest($newProductData);*/
$dp->endImportSession();
?>


