<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit3add8f2d82784982773c9df4e5f67cf3
{
    public static $files = array (
        '0e6d7bf4a5811bfa5cf40c5ccd6fae6a' => __DIR__ . '/..' . '/symfony/polyfill-mbstring/bootstrap.php',
    );

    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Symfony\\Polyfill\\Mbstring\\' => 26,
            'Symfony\\Component\\Debug\\' => 24,
            'Symfony\\Component\\Console\\' => 26,
        ),
        'P' => 
        array (
            'Psr\\Log\\' => 8,
        ),
        'J' => 
        array (
            'JsonSchema\\' => 11,
        ),
        'E' => 
        array (
            'Eloquent\\Enumeration\\' => 21,
            'Eloquent\\Composer\\Configuration\\' => 32,
        ),
        'A' => 
        array (
            'AydinHassan\\MagentoCoreComposerInstaller\\' => 41,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Symfony\\Polyfill\\Mbstring\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-mbstring',
        ),
        'Symfony\\Component\\Debug\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/debug',
        ),
        'Symfony\\Component\\Console\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/console',
        ),
        'Psr\\Log\\' => 
        array (
            0 => __DIR__ . '/..' . '/psr/log/Psr/Log',
        ),
        'JsonSchema\\' => 
        array (
            0 => __DIR__ . '/..' . '/justinrainbow/json-schema/src/JsonSchema',
        ),
        'Eloquent\\Enumeration\\' => 
        array (
            0 => __DIR__ . '/..' . '/eloquent/enumeration/src',
        ),
        'Eloquent\\Composer\\Configuration\\' => 
        array (
            0 => __DIR__ . '/..' . '/eloquent/composer-config-reader/src',
        ),
        'AydinHassan\\MagentoCoreComposerInstaller\\' => 
        array (
            0 => __DIR__ . '/..' . '/aydin-hassan/magento-core-composer-installer/src',
        ),
    );

    public static $prefixesPsr0 = array (
        'M' => 
        array (
            'MagentoHackathon\\Composer' => 
            array (
                0 => __DIR__ . '/..' . '/magento-hackathon/magento-composer-installer/src',
            ),
        ),
    );

    public static $classMap = array (
        'TheSeer\\Autoload\\Application' => __DIR__ . '/..' . '/theseer/autoload/src/Application.php',
        'TheSeer\\Autoload\\ApplicationException' => __DIR__ . '/..' . '/theseer/autoload/src/Application.php',
        'TheSeer\\Autoload\\AutoloadBuilderException' => __DIR__ . '/..' . '/theseer/autoload/src/AutoloadRenderer.php',
        'TheSeer\\Autoload\\AutoloadRenderer' => __DIR__ . '/..' . '/theseer/autoload/src/AutoloadRenderer.php',
        'TheSeer\\Autoload\\CLI' => __DIR__ . '/..' . '/theseer/autoload/src/CLI.php',
        'TheSeer\\Autoload\\CLIEnvironmentException' => __DIR__ . '/..' . '/theseer/autoload/src/CLI.php',
        'TheSeer\\Autoload\\Cache' => __DIR__ . '/..' . '/theseer/autoload/src/Cache.php',
        'TheSeer\\Autoload\\CacheEntry' => __DIR__ . '/..' . '/theseer/autoload/src/CacheEntry.php',
        'TheSeer\\Autoload\\CacheException' => __DIR__ . '/..' . '/theseer/autoload/src/Cache.php',
        'TheSeer\\Autoload\\CacheWarmingListRenderer' => __DIR__ . '/..' . '/theseer/autoload/src/CacheWarmingListRenderer.php',
        'TheSeer\\Autoload\\CachingParser' => __DIR__ . '/..' . '/theseer/autoload/src/CachingParser.php',
        'TheSeer\\Autoload\\ClassDependencySorter' => __DIR__ . '/..' . '/theseer/autoload/src/DependencySorter.php',
        'TheSeer\\Autoload\\ClassDependencySorterException' => __DIR__ . '/..' . '/theseer/autoload/src/DependencySorter.php',
        'TheSeer\\Autoload\\Collector' => __DIR__ . '/..' . '/theseer/autoload/src/Collector.php',
        'TheSeer\\Autoload\\CollectorException' => __DIR__ . '/..' . '/theseer/autoload/src/Collector.php',
        'TheSeer\\Autoload\\CollectorResult' => __DIR__ . '/..' . '/theseer/autoload/src/CollectorResult.php',
        'TheSeer\\Autoload\\CollectorResultException' => __DIR__ . '/..' . '/theseer/autoload/src/CollectorResult.php',
        'TheSeer\\Autoload\\ComposerIterator' => __DIR__ . '/..' . '/theseer/autoload/src/ComposerIterator.php',
        'TheSeer\\Autoload\\ComposerIteratorException' => __DIR__ . '/..' . '/theseer/autoload/src/ComposerIterator.php',
        'TheSeer\\Autoload\\Config' => __DIR__ . '/..' . '/theseer/autoload/src/Config.php',
        'TheSeer\\Autoload\\Factory' => __DIR__ . '/..' . '/theseer/autoload/src/Factory.php',
        'TheSeer\\Autoload\\Logger' => __DIR__ . '/..' . '/theseer/autoload/src/Logger.php',
        'TheSeer\\Autoload\\ParseResult' => __DIR__ . '/..' . '/theseer/autoload/src/ParseResult.php',
        'TheSeer\\Autoload\\Parser' => __DIR__ . '/..' . '/theseer/autoload/src/Parser.php',
        'TheSeer\\Autoload\\ParserException' => __DIR__ . '/..' . '/theseer/autoload/src/Parser.php',
        'TheSeer\\Autoload\\ParserInterface' => __DIR__ . '/..' . '/theseer/autoload/src/ParserInterface.php',
        'TheSeer\\Autoload\\PathComparator' => __DIR__ . '/..' . '/theseer/autoload/src/PathComparator.php',
        'TheSeer\\Autoload\\PharBuilder' => __DIR__ . '/..' . '/theseer/autoload/src/PharBuilder.php',
        'TheSeer\\Autoload\\SourceFile' => __DIR__ . '/..' . '/theseer/autoload/src/SourceFile.php',
        'TheSeer\\Autoload\\StaticListRenderer' => __DIR__ . '/..' . '/theseer/autoload/src/StaticListRenderer.php',
        'TheSeer\\Autoload\\StaticRenderer' => __DIR__ . '/..' . '/theseer/autoload/src/StaticRenderer.php',
        'TheSeer\\Autoload\\StaticRequireListRenderer' => __DIR__ . '/..' . '/theseer/autoload/src/StaticRequireListRenderer.php',
        'TheSeer\\Autoload\\Version' => __DIR__ . '/..' . '/theseer/autoload/src/Version.php',
        'TheSeer\\DirectoryScanner\\DirectoryScanner' => __DIR__ . '/..' . '/theseer/directoryscanner/src/directoryscanner.php',
        'TheSeer\\DirectoryScanner\\Exception' => __DIR__ . '/..' . '/theseer/directoryscanner/src/directoryscanner.php',
        'TheSeer\\DirectoryScanner\\FilesOnlyFilterIterator' => __DIR__ . '/..' . '/theseer/directoryscanner/src/filesonlyfilter.php',
        'TheSeer\\DirectoryScanner\\IncludeExcludeFilterIterator' => __DIR__ . '/..' . '/theseer/directoryscanner/src/includeexcludefilter.php',
        'TheSeer\\DirectoryScanner\\PHPFilterIterator' => __DIR__ . '/..' . '/theseer/directoryscanner/src/phpfilter.php',
        'ezcBase' => __DIR__ . '/..' . '/zetacomponents/base/src/base.php',
        'ezcBaseAutoloadException' => __DIR__ . '/..' . '/zetacomponents/base/src/exceptions/autoload.php',
        'ezcBaseAutoloadOptions' => __DIR__ . '/..' . '/zetacomponents/base/src/options/autoload.php',
        'ezcBaseConfigurationInitializer' => __DIR__ . '/..' . '/zetacomponents/base/src/interfaces/configuration_initializer.php',
        'ezcBaseDoubleClassRepositoryPrefixException' => __DIR__ . '/..' . '/zetacomponents/base/src/exceptions/double_class_repository_prefix.php',
        'ezcBaseException' => __DIR__ . '/..' . '/zetacomponents/base/src/exceptions/exception.php',
        'ezcBaseExportable' => __DIR__ . '/..' . '/zetacomponents/base/src/interfaces/exportable.php',
        'ezcBaseExtensionNotFoundException' => __DIR__ . '/..' . '/zetacomponents/base/src/exceptions/extension_not_found.php',
        'ezcBaseFeatures' => __DIR__ . '/..' . '/zetacomponents/base/src/features.php',
        'ezcBaseFile' => __DIR__ . '/..' . '/zetacomponents/base/src/file.php',
        'ezcBaseFileException' => __DIR__ . '/..' . '/zetacomponents/base/src/exceptions/file_exception.php',
        'ezcBaseFileFindContext' => __DIR__ . '/..' . '/zetacomponents/base/src/structs/file_find_context.php',
        'ezcBaseFileIoException' => __DIR__ . '/..' . '/zetacomponents/base/src/exceptions/file_io.php',
        'ezcBaseFileNotFoundException' => __DIR__ . '/..' . '/zetacomponents/base/src/exceptions/file_not_found.php',
        'ezcBaseFilePermissionException' => __DIR__ . '/..' . '/zetacomponents/base/src/exceptions/file_permission.php',
        'ezcBaseFunctionalityNotSupportedException' => __DIR__ . '/..' . '/zetacomponents/base/src/exceptions/functionality_not_supported.php',
        'ezcBaseInit' => __DIR__ . '/..' . '/zetacomponents/base/src/init.php',
        'ezcBaseInitCallbackConfiguredException' => __DIR__ . '/..' . '/zetacomponents/base/src/exceptions/init_callback_configured.php',
        'ezcBaseInitInvalidCallbackClassException' => __DIR__ . '/..' . '/zetacomponents/base/src/exceptions/invalid_callback_class.php',
        'ezcBaseInvalidParentClassException' => __DIR__ . '/..' . '/zetacomponents/base/src/exceptions/invalid_parent_class.php',
        'ezcBaseMetaData' => __DIR__ . '/..' . '/zetacomponents/base/src/metadata.php',
        'ezcBaseMetaDataPearReader' => __DIR__ . '/..' . '/zetacomponents/base/src/metadata/pear.php',
        'ezcBaseMetaDataTarballReader' => __DIR__ . '/..' . '/zetacomponents/base/src/metadata/tarball.php',
        'ezcBaseOptions' => __DIR__ . '/..' . '/zetacomponents/base/src/options.php',
        'ezcBasePersistable' => __DIR__ . '/..' . '/zetacomponents/base/src/interfaces/persistable.php',
        'ezcBasePropertyNotFoundException' => __DIR__ . '/..' . '/zetacomponents/base/src/exceptions/property_not_found.php',
        'ezcBasePropertyPermissionException' => __DIR__ . '/..' . '/zetacomponents/base/src/exceptions/property_permission.php',
        'ezcBaseRepositoryDirectory' => __DIR__ . '/..' . '/zetacomponents/base/src/structs/repository_directory.php',
        'ezcBaseSettingNotFoundException' => __DIR__ . '/..' . '/zetacomponents/base/src/exceptions/setting_not_found.php',
        'ezcBaseSettingValueException' => __DIR__ . '/..' . '/zetacomponents/base/src/exceptions/setting_value.php',
        'ezcBaseStruct' => __DIR__ . '/..' . '/zetacomponents/base/src/struct.php',
        'ezcBaseValueException' => __DIR__ . '/..' . '/zetacomponents/base/src/exceptions/value.php',
        'ezcBaseWhateverException' => __DIR__ . '/..' . '/zetacomponents/base/src/exceptions/whatever.php',
        'ezcConsoleArgument' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/input/argument.php',
        'ezcConsoleArgumentAlreadyRegisteredException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/argument_already_registered.php',
        'ezcConsoleArgumentException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/argument.php',
        'ezcConsoleArgumentMandatoryViolationException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/argument_mandatory_violation.php',
        'ezcConsoleArgumentTypeViolationException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/argument_type_violation.php',
        'ezcConsoleArguments' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/input/arguments.php',
        'ezcConsoleDialog' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/interfaces/dialog.php',
        'ezcConsoleDialogAbortException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/dialog_abort.php',
        'ezcConsoleDialogOptions' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/options/dialog.php',
        'ezcConsoleDialogValidator' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/interfaces/dialog_validator.php',
        'ezcConsoleDialogViewer' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/dialog_viewer.php',
        'ezcConsoleException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/exception.php',
        'ezcConsoleInput' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/input.php',
        'ezcConsoleInputHelpGenerator' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/interfaces/input_help_generator.php',
        'ezcConsoleInputStandardHelpGenerator' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/input/help_generators/standard.php',
        'ezcConsoleInputValidator' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/interfaces/input_validator.php',
        'ezcConsoleInvalidOptionNameException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/invalid_option_name.php',
        'ezcConsoleInvalidOutputTargetException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/invalid_output_target.php',
        'ezcConsoleMenuDialog' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/dialog/menu_dialog.php',
        'ezcConsoleMenuDialogDefaultValidator' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/dialog/validators/menu_dialog_default.php',
        'ezcConsoleMenuDialogOptions' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/options/menu_dialog.php',
        'ezcConsoleMenuDialogValidator' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/interfaces/menu_dialog_validator.php',
        'ezcConsoleNoPositionStoredException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/no_position_stored.php',
        'ezcConsoleNoValidDialogResultException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/no_valid_dialog_result.php',
        'ezcConsoleOption' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/input/option.php',
        'ezcConsoleOptionAlreadyRegisteredException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/option_already_registered.php',
        'ezcConsoleOptionArgumentsViolationException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/option_arguments_violation.php',
        'ezcConsoleOptionDependencyViolationException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/option_dependency_violation.php',
        'ezcConsoleOptionException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/option.php',
        'ezcConsoleOptionExclusionViolationException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/option_exclusion_violation.php',
        'ezcConsoleOptionMandatoryViolationException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/option_mandatory_violation.php',
        'ezcConsoleOptionMissingValueException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/option_missing_value.php',
        'ezcConsoleOptionNoAliasException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/option_no_alias.php',
        'ezcConsoleOptionNotExistsException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/option_not_exists.php',
        'ezcConsoleOptionRule' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/structs/option_rule.php',
        'ezcConsoleOptionStringNotWellformedException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/option_string_not_wellformed.php',
        'ezcConsoleOptionTooManyValuesException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/option_too_many_values.php',
        'ezcConsoleOptionTypeViolationException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/option_type_violation.php',
        'ezcConsoleOutput' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/output.php',
        'ezcConsoleOutputFormat' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/structs/output_format.php',
        'ezcConsoleOutputFormats' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/structs/output_formats.php',
        'ezcConsoleOutputOptions' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/options/output.php',
        'ezcConsoleProgressMonitor' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/progressmonitor.php',
        'ezcConsoleProgressMonitorOptions' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/options/progressmonitor.php',
        'ezcConsoleProgressbar' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/progressbar.php',
        'ezcConsoleProgressbarOptions' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/options/progressbar.php',
        'ezcConsoleQuestionDialog' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/dialog/question_dialog.php',
        'ezcConsoleQuestionDialogCollectionValidator' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/dialog/validators/question_dialog_collection.php',
        'ezcConsoleQuestionDialogMappingValidator' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/dialog/validators/question_dialog_mapping.php',
        'ezcConsoleQuestionDialogOptions' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/options/question_dialog.php',
        'ezcConsoleQuestionDialogRegexValidator' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/dialog/validators/question_dialog_regex.php',
        'ezcConsoleQuestionDialogTypeValidator' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/dialog/validators/question_dialog_type.php',
        'ezcConsoleQuestionDialogValidator' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/interfaces/question_dialog_validator.php',
        'ezcConsoleStandardInputValidator' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/input/validators/standard.php',
        'ezcConsoleStatusbar' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/statusbar.php',
        'ezcConsoleStatusbarOptions' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/options/statusbar.php',
        'ezcConsoleStringTool' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/tools/string.php',
        'ezcConsoleTable' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/table.php',
        'ezcConsoleTableCell' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/table/cell.php',
        'ezcConsoleTableOptions' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/options/table.php',
        'ezcConsoleTableRow' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/table/row.php',
        'ezcConsoleTooManyArgumentsException' => __DIR__ . '/..' . '/zetacomponents/console-tools/src/exceptions/argument_too_many.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit3add8f2d82784982773c9df4e5f67cf3::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit3add8f2d82784982773c9df4e5f67cf3::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInit3add8f2d82784982773c9df4e5f67cf3::$prefixesPsr0;
            $loader->classMap = ComposerStaticInit3add8f2d82784982773c9df4e5f67cf3::$classMap;

        }, null, ClassLoader::class);
    }
}
